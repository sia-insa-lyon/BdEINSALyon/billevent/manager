import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Order, OrdersService} from '../../data/orders.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  order: Order;

  constructor(private currentRoute: ActivatedRoute, private ordersService: OrdersService) {
  }

  ngOnInit() {
    const id = this.currentRoute.snapshot.params['id'];
    this.ordersService.get(id).subscribe((order) => {
      this.order = order;
    });
  }

}
