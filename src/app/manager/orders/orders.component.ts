import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  config = {
    endpoint: environment.BilleventApi + '/admin/orders/',
    columns: [
      {
        title: '#',
        field: 'id',
        type: 'string'
      },
      {
        title: 'Client',
        field: 'client_name',
        type: 'string'
      },
      {
        title: 'Montant',
        field: 'amount',
        type: 'currency'
      },
      {
        title: 'Date',
        field: 'created_at',
        type: 'date'
      },
      {
        title: 'Status',
        field: 'status',
        type: 'order-status'
      }
    ],
    params: {
      status: 'accountable'
    }
  };

  constructor(private currentRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  open(entry: any) {
    console.log(entry);
    this.router.navigate(['..', 'orders', entry.id], {relativeTo: this.currentRoute});
  }

}
