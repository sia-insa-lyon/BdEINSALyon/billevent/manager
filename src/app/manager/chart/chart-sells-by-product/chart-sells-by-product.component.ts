import {Component, Input, OnInit} from '@angular/core';
import {BilletsService} from '../../data/billets.service';
import {BilleventEvent, EventsService, Product} from '../../data/events.service';

@Component({
  selector: 'app-chart-sells-by-product',
  templateUrl: './chart-sells-by-product.component.html',
  styleUrls: ['./chart-sells-by-product.component.scss']
})
export class ChartSellsByProductComponent implements OnInit {

  @Input() event: BilleventEvent;

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Country';
  showYAxisLabel = false;
  yAxisLabel = 'Population';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  products: Product[];
  data: any;

  constructor(private billetsService: BilletsService, private eventService: EventsService) {
  }

  ngOnInit() {
    this.eventService.getProducts(this.event).subscribe(
      (products) => {
        this.products = products;
        this.refreshData();
      }
    );
  }

  refreshData() {
    this.billetsService.countSoldByDay(
      this.event,
      this.products.filter((v: any) => !v.checked).map((v) => v.id)
    ).subscribe((d) => {
      const counts = d.counts;
      const data = {
        'name': 'Places',
        'series': counts.map((c) => ({name: new Date(c.day), value: c.total}) )
      };
      this.data = [data];
      console.log(this.data);
    } );
  }

}
