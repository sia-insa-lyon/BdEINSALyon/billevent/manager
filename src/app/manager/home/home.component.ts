import { Component, OnInit } from '@angular/core';
import {BilleventEvent, EventsService, Product} from '../data/events.service';
import {ActivatedRoute} from '@angular/router';
import {BilletsService} from '../data/billets.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  event: BilleventEvent;

  products: Product[];
  data: any = {};
  loading = true;

  constructor(private eventsService: EventsService,
              private route: ActivatedRoute,
              private billetsService: BilletsService) { }

  ngOnInit() {
    this.route.parent.params.subscribe((params) => {
      this.eventsService.getEvent(params.event_id).subscribe((event) => {
        this.event = event;
        this.eventsService.getProducts(this.event).subscribe(
          (products) => {
            this.products = products;
            this.refreshData();
          }
        );
      });
    });
  }

  refreshData() {
    this.loading = true;
    const dates: number[] = [];
    Observable.forkJoin(...this.products.map((product) => {
      return this.billetsService.countSoldByDay(
        this.event,
        [product.id]
      ).map((d) => {
        const counts = d.counts;
        const data = {};
        let total = 0;
        counts.forEach((c) => {
          const time = new Date(c.day);
          if (dates.indexOf(time.getTime()) < 0) {
            dates.push(time.getTime());
          }
          data[time.getTime()] = c.total;
          total += c.total;
        });
        return {product, data, total};
      });
    })).subscribe((result) => {
      dates.sort((a, b) => {
        return a - b;
      });
      this.data = result;
      this.loading = false;
    });
  }

}
