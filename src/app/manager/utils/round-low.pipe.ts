import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundLow'
})
export class RoundLowPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Math.trunc(value);
  }

}
