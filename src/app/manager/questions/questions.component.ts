import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  config = {
    endpoint: environment.BilleventApi + '/admin/questions/',
    columns: [
      {
        title: '#',
        field: 'id',
        type: 'string'
      },
      {
        title: 'Question',
        field: 'question',
        type: 'string'
      },
      {
        title: 'Cible',
        field: 'target',
        type: 'string'
      }
    ],
    params: {}
  };

  constructor(private currentRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  open(entry: any) {
    console.log(entry);
    this.router.navigate(['..', 'questions', entry.id], {relativeTo: this.currentRoute});
  }

}
