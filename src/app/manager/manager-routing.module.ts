import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './auth-guard.service';
import {InvitationComponent} from './invitation/invitation.component';
import {HomeComponent} from './home/home.component';
import {NewInvitationComponent} from './invitation/new-invitation/new-invitation.component';
import {EditInvitationComponent} from './invitation/edit-invitation/edit-invitation.component';
import {WelcomeControllerComponent} from './welcome-controller/welcome-controller.component';
import {OrdersComponent} from './orders/orders.component';
import {OrderComponent} from './orders/order/order.component';
import {QuestionsComponent} from './questions/questions.component';
import {QuestionComponent} from './questions/question/question.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomeControllerComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':event_id',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'invitations',
        component: InvitationComponent
      },
      {
        path: 'invitations/new',
        component: NewInvitationComponent
      },
      {
        path: 'invitations/:id',
        component: EditInvitationComponent
      },
      {
        path: 'orders',
        component: OrdersComponent
      },
      {
        path: 'orders/:id',
        component: OrderComponent
      },
      {
        path: 'questions',
        component: QuestionsComponent
      },
      {
        path: 'questions/:id',
        component: QuestionComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class ManagerRoutingModule { }
